## Программа
* [Учебная пограмма](https://bitbucket.org/strymm/web-programming-course/downloads/WP_cirriculum.docx)

## Лекции
* [0. Введение в курс](https://bitbucket.org/strymm/web-programming-course/downloads/WP-0.%20Introduction%20to%20course.pptx)
* [1. Введение в web-программирование](https://bitbucket.org/strymm/web-programming-course/downloads/WP-1.%20Introduction%20to%20web%20programming.pptx)

## Лабораторный практикум
* [Задания](https://bitbucket.org/strymm/web-programming-course/downloads/WP_lab_practice.docx)

## Дополнительные материалы

* [Bitbucket Tutorials: Teams in Space Training Ground](https://confluence.atlassian.com/bitbucket/bitbucket-tutorials-teams-in-space-training-ground-755338051.html)
* [Bitbucket: Создание приватного репозитория](https://habrahabr.ru/sandbox/37865)
